# **Cozmo Driver for ROS**

This is an **unofficial** ROS node for Anki Cozmo.

![alt text](https://store.storeimages.cdn-apple.com/4981/as-images.apple.com/is/image/AppleInc/aos/published/images/H/KE/HKE32/HKE32?wid=572&hei=572&fmt=jpeg&qlt=95&op_usm=0.5,0.5&.v=1477599202279)
## Requirements

This package assumes compatability with the following:
 * Ubuntu 14.04/16.04
 * ROS Indigo/Kinetic
 * iOS Cozmo Mobile App (Android to be added at a later time)

## Installation
Use install.sh to install all the required packages/dependencies with the path of your catkin workspace and ROS Distribution as a command line arguments to the bash script:
```sh
sudo ./install.sh /path/to/catkin_workspace ROS_VERSION
```

This shell script will install:
 * Python3.5
 * pip3
 * Cozmo SDK 0.10+
 * USB packages for iOS mobile devices

# Topics / Transformations

### Publish
 * /cozmo_camera/image (sensor_msgs/Image) : camera image from cozmo. This is gray scale, but the format is rgb8.
 * /cozmo_camera/camera_info (sensor_msgs/CameraInfo) : The camera info from Cozmo's camera ( **Note: I did a very poor calibration!** ).
 * /joint_states (sensor_msgs/JointState) : This contains the head angle [rad] and the lift height [m]
 * /imu (sensor_msgs/Imu) : Imu mounted on cozmo head
 * /battery (sensor_msgs/BatteryState) : battery voltage and charging status
 * /diagnostics (diagnostic_msgs/DiagnosticArray) : Several robot diagnostics.

Note: Cozmo SDK will become idle mode if the message is not sent to cozmo for a few minutes. To avoid idle mode, cozmo_driver.py is sending /cmd_vel repeatedly in 10[Hz].

### Subscribe
 * /cmd_vel (geometry_msgs/Twist) : command velocity as usual. ( **Note: velocity is correct now!** )
 * /say (std_msgs/String) : cozmo says this text
 * /head_angle (std_msgs/Float64) : command head angle [rad]
 * /lift_height (std_msgs/Float64) : command lift height (value between 0 and 1, which is autoscaled by Cozmo SDK) [float]
 * /backpack_led (std_msgs/ColorRGBA) : led color on backpack

### Transformations provided
 * odom_frame -> footprint_frame
 * footprint_frame -> base_frame
 * base_frame -> head_frame
 * head_frame -> camera_frame
 * camera_frame -> camera_optical_frame
 * odom_frame -> cube_xx (for detected cubes)

# Hardware configuration

It is normal for Cozmo SDK, but it has below hardware configuration. ROS is running on your **PC** not on Cozmo.

**Cozmo** (WiFi station) <-- WiFi --> **Phone** (iOS) <-- USB cable --> **PC** (Cozmo official SDK <-> ROS)

# Demo Applications

## Important Note

***Before connecting an iOS device and running or launching a node in this package, make sure to run in another terminal window:***
```sh
usbmuxd -X
sudo usbmuxd -f
```

## Cozmo Driver

This demo application starts the Cozmo ROS driver, a keyboard teleoperation node and a twist multiplexing node. Additionally rviz is started for visiualization.
Optionally joystick input can be used with argument "use_joy".

```sh
roslaunch cozmo_driver demo.launch
```

To enable joystick use:
```
roslaunch cozmo_driver demo.launch use_joy:=true
```

## RatSLAM

**Note:** To get the RatSLAM demo working you need to build the sources from [here](https://github.com/sem23/ratslam.git) in your ROS workspace.

This demo application starts a SLAM system utilizing a biological inspired SLAM, called RatSLAM. In a few words, it is a
monocular visiual SLAM building an experience map from odometry and image information. The provided version supports a simple
navigation system using a naive path follow controller. So be careful while using it, as neither Cozmo nor the path controller
provide collision avoidance!

```sh
roslaunch cozmo_driver ratslam.launch
```

