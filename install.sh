#! /usr/bin/env bash

VALID_DISTROS="indigo kinetic melodic"

# Error Checking
if [ -z "$1" ] || [ -z "$2" ]; then
	echo "Usage: $0 /path/to/catkin_workspace ROS_VERSION"
	exit 1
fi

if ! echo $VALID_DISTROS | grep -w $2 > /dev/null; then
	echo "$0: '$2' is an invalid ROS Distribution. Valid: $VALID_DISTROS"
	exit 1
fi

if [ ! -d $1 ]; 
    then
        echo "$0: directory '$1' doesn't exist"
        exit 1
    else
        cd $1
        if [ ! -d "build" ] || [ ! -d "src" ] || [ ! -d "devel" ]; then
    		echo "$0: '$1' is not a catkin workspace"
    		exit 1
        fi
fi

# Install ROS Dependencies and Make
printf "=================================================\nInstalling ROS Dependencies and Making...\n=================================================\n"
source /opt/ros/$2/setup.bash
catkin_make
source devel/setup.bash
rosdep install cozmo_driver
roscd cozmo_driver


# Install apt-get dependencies
printf "=================================================\nInstalling apt-get dependencies...\n=================================================\n"
sudo apt-get update
sudo apt-get install python3-pip python3-venv python-numpy python3-yaml ros-$2-perception pkg-config libtool-bin autotools-dev autoconf automake python3-pil.imagetk usbmuxd

# Install virtual python environment
printf "=================================================\nInstalling virtualenv...\n=================================================\n"
if [ ! -d envs ]; then
    mkdir envs
fi
cd envs
python3 -m venv venv
source venv/bin/activate
pip3 install rospkg catkin_pkg pyyaml

# Install Cozmo SDK and dependencies
printf "=================================================\nInstalling Cozmo SDK...\n=================================================\n"
pip3 install 'cozmo[camera]'
deactivate
pip install scipy pykalman future --user
cd ..

